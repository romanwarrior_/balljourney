using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GlobalEvents : MonoBehaviour
{
    public UnityEvent OnGameStarted;
    public UnityEvent OnPlayerDied;
    public UnityEvent OnGameRestarted;

    #region Singleton
    private static GlobalEvents instance;
    public static GlobalEvents Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<GlobalEvents>();
            return instance;
        }
    }
    #endregion
}
