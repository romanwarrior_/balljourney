using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InGameUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI hpText;
    [SerializeField] private TextMeshProUGUI speedText;
    [SerializeField] private GameObject defeatWindow;

    #region Singleton
    private static InGameUI instance;
    public static InGameUI Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<InGameUI>();
            return instance;
        }
    }
    #endregion

    private void Start()
    {
        UpdateUI(1);
        GlobalEvents.Instance.OnPlayerDied.AddListener(OnPlayerDied);
    }

    public void UpdateUI(float speed)
    {
        speedText.text = $"Speed : x{Math.Round(speed,2)}";
    }

    public void UpdateUI(int score, int hp)
    {
        scoreText.text = $"Score : {score}";
        hpText.text = $"Hp : {hp}";
    }
    private void OnPlayerDied()
    {
        defeatWindow.SetActive(true);
    }
}
