using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartGameButton : MonoBehaviour
{
    public void Click() 
    {
        GlobalEvents.Instance.OnGameRestarted?.Invoke();
        transform.parent.gameObject.SetActive(false);
    }
}
