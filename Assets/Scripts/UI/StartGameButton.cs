using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameButton : MonoBehaviour
{
    public void Click()
    {
        GlobalEvents.Instance.OnGameStarted?.Invoke();
        transform.parent.gameObject.SetActive(false);
    }
}
