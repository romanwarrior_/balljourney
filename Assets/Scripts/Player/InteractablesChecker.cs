using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablesChecker : MonoBehaviour
{
    [SerializeField] private Player player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Obstacle obstacle))
        {
            player.GetDamage(obstacle.Damage);
            obstacle.Destroy();
        }
        if (other.TryGetComponent(out ScoreObject score))
        {
            player.GetScore(score.Value);
            score.Destroy();
        }
    }
}
