using DG.Tweening;
using UniRx;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Player player;

    private float offsetX;

    private Vector3 StartPosition => new Vector3(0, -10, -4);

    private void Start()
    {
        GlobalEvents.Instance.OnGameRestarted.AddListener(OnGameStarted);
    }

    private void Update()
    {
        offsetX = InputHandler.Instance.OffsetX * new Vector3(1, 0, 0).x * Time.deltaTime;

        CheckWallCollision();

        if(InputHandler.Instance.InputEnabled) transform.position += new Vector3(1, 0, 0) * offsetX;

    }

    private void CheckWallCollision()
    {
        if (Mathf.Abs(transform.position.x) > 4 && InputHandler.Instance.InputEnabled)
        {
            player.GetDamage(1);

            InputHandler.Instance.DisableInput();
            offsetX = 0;

            transform.DOMoveX(transform.position.x > 0 ? 3 : -3, 0.5f);

            Observable.Timer(new System.TimeSpan(0, 0, 0, 0, 550)).TakeUntilDestroy(gameObject).Subscribe(_ =>
            {
                if (player.Hp > 0)
                { 
                    InputHandler.Instance.EnableInput(); 
                }
            });
        }
    }

    private void OnGameStarted()
    {
        transform.position = StartPosition;
    }
}
