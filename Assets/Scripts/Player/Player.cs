using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] private SpeedSetter speed;
    [SerializeField] private int startHpCount;
    [SerializeField] private int[] scoreAmountToSpeedUp;
    [SerializeField] private float[] speedUpMultiplier;

    public int Hp => hpCount;

    private int score;
    private int hpCount;

    private void Start()
    {
        hpCount = startHpCount;
        score = 0;
        InGameUI.Instance.UpdateUI(score, hpCount);

        GlobalEvents.Instance.OnGameRestarted.AddListener(OnGameStarted);
    }

    public void GetDamage(int value)
    {
        hpCount -= value;
        if (hpCount <= 0)
        {
            Die();
        }
        InGameUI.Instance.UpdateUI(score,hpCount);
    }

    public void GetScore(int value)
    {
        score += value;
        InGameUI.Instance.UpdateUI(score, hpCount);

        float currentMultiplier = 1;
        for(int i = 0; i < scoreAmountToSpeedUp.Length; i++)
        {
            if (score > scoreAmountToSpeedUp[i]) currentMultiplier = speedUpMultiplier[i];
        }
        speed.SpeedUpAllInteractables(currentMultiplier);
    }

    private void Die()
    {
        speed.SpeedUpAllInteractables(0);
        GlobalEvents.Instance.OnPlayerDied?.Invoke();
    }

    private void OnGameStarted()
    {
        score = 0;
        hpCount = 3;
        InGameUI.Instance.UpdateUI(score, hpCount);
        InGameUI.Instance.UpdateUI(1);
    }
}
