using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedSetter : MonoBehaviour
{
    [SerializeField] private InteractablesSpawner spawner;

    public const float SpeedIncreasionSpeed = 0.8f;

    public float RequiredSpeed => requiredSpeed;
    public float CurrentSpeed => currentSpeed;

    private float valueBeforeIncrease;
    private float requiredSpeed;
    private float currentSpeed;
    private float t;

    private void Start()
    {
        GlobalEvents.Instance.OnPlayerDied.AddListener(StopAll);
        GlobalEvents.Instance.OnGameRestarted.AddListener(OnGameStarted);

        OnGameStarted();
    }
    private void Update()
    {
        t += Time.deltaTime * SpeedIncreasionSpeed;
        if (currentSpeed < requiredSpeed)
        {
            currentSpeed = Mathf.Lerp(valueBeforeIncrease, requiredSpeed, t);
            InGameUI.Instance.UpdateUI(currentSpeed);
        }
    }

    public void SpeedUpAllInteractables(float requiredSpeed)
    {
        t = 0;
        valueBeforeIncrease = currentSpeed;
        this.requiredSpeed = requiredSpeed;
        foreach (var interactable in spawner.Interactables)
        {
            interactable.SpeedUp(currentSpeed, requiredSpeed);
        }
    }

    public void SpeedUpInteractable(BaseInteractable interactable, float requiredSpeed)
    {
        t = 0;
        valueBeforeIncrease = currentSpeed;
        this.requiredSpeed = requiredSpeed;
        interactable.SpeedUp(currentSpeed,requiredSpeed);
    }

    private void StopAll()
    {
        currentSpeed = requiredSpeed = 0;
        foreach (var interactable in spawner.Interactables)
        {
            interactable.Stop();
        }
    }

    private void OnGameStarted()
    {
        currentSpeed = 1;
        requiredSpeed = 1;
    }
}
