using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField] private float sensitivity;

    public bool InputEnabled => inputEnabled;

    private bool inputEnabled;

    public float OffsetX => offsetX * sensitivity;

    private float currentTapX;
    private float previousTapX;
    private float offsetX;

    #region Singleton
    private static InputHandler instance;
    public static InputHandler Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<InputHandler>();
            return instance;
        }
    }
    #endregion

    private void Start()
    {
        GlobalEvents.Instance.OnPlayerDied.AddListener(DisableInput);
        GlobalEvents.Instance.OnGameRestarted.AddListener(EnableInput);
        GlobalEvents.Instance.OnGameStarted.AddListener(EnableInput);

        DisableInput();
        EnableInput();
    }

    private void Update()
    {
        if (!inputEnabled) return;

        if (Input.GetMouseButtonDown(0))
        {
            previousTapX = Input.mousePosition.x;
        }

        if (Input.GetMouseButton(0))
        {
            currentTapX = Input.mousePosition.x;
            offsetX = currentTapX - previousTapX;
            previousTapX = currentTapX;
        }
        if (Input.GetMouseButtonUp(0))
        {
            currentTapX = previousTapX;
        }
    }

    public void EnableInput()
    {
        previousTapX = Input.mousePosition.x;
        offsetX = 0;
        inputEnabled = true;
    }

    public void DisableInput()
    {
        inputEnabled = false;
    }


}
