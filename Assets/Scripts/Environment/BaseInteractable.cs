using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityNightPool;

public class BaseInteractable : MonoBehaviour
{
    [SerializeField] private float startSpeed;

    private float requiredSpeed;
    private float currentSpeed;
    private float valueBeforeIncrease;
    private float t;

    private const float LetalOffsetY = -22f;

    private float Speed => startSpeed * currentSpeed;

    private void Start()
    {
        GlobalEvents.Instance.OnGameRestarted.AddListener(Destroy);
    }

    private void Update()
    {
        if (currentSpeed < requiredSpeed)
        {
            currentSpeed = Mathf.Lerp(currentSpeed, requiredSpeed, Time.deltaTime * SpeedSetter.SpeedIncreasionSpeed);
        }

        Move();
    }

    private void Move()
    {
        if (transform.position.y > LetalOffsetY)
        {
            transform.position -= new Vector3(0, Speed * Time.deltaTime, 0);
        }
        else
        {
            Destroy();
        }
    }

    public void SpeedUp(float currentValue, float requiredValue)
    {
        currentSpeed = currentValue;
        requiredSpeed = requiredValue;
    }

    public void Destroy()
    {
        GetComponent<PoolObject>().Return();
    }

    public void Stop()
    {
        currentSpeed = requiredSpeed = 0;
    }
}
