using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreObject : BaseInteractable
{
    [SerializeField] private int value;

    public int Value => value;
}
