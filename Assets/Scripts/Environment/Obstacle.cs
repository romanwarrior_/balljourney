using UnityEngine;
using UnityNightPool;

public class Obstacle : BaseInteractable
{
    [SerializeField] private int damage;
    public int Damage => damage;

}
