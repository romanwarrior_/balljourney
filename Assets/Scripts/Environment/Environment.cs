using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    private float[] wallsOffset = { -8, 8 };

    public float[] WallsOffset => wallsOffset;

    #region Singleton
    private static Environment instance;
    public static Environment Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<Environment>();
            return instance;
        }
    }
    #endregion

}
