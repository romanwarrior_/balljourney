using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityNightPool;

public class InteractablesSpawner : MonoBehaviour
{
    [SerializeField] private int spawnInterval;
    [SerializeField] private SpeedSetter speedSetter;

    public List<BaseInteractable> Interactables => interactables;

    private List<BaseInteractable> interactables = new List<BaseInteractable>();

    private const float ObstacleWidth = 3;
    private const float WallWidth = 4;

    private const float SpawnY = 30;
    private const float SpawnZ = -ObstacleWidth;

    private float[] SpawnLimits
    {
        get
        {
            return new float[]
            {
                Environment.Instance.WallsOffset[0]+(ObstacleWidth+WallWidth)/2,
                Environment.Instance.WallsOffset[1]-(ObstacleWidth+WallWidth)/2,
            };
        }
    }

    private void Start()
    {
        GlobalEvents.Instance.OnGameStarted.AddListener(StartSpawning);
    }


    private void StartSpawning()
    {
        SpawnInteractable(1);

        Observable.Timer(new TimeSpan(0, 0, spawnInterval / 2)).TakeUntilDestroy(gameObject).Subscribe(_ =>
        {
            SpawnInteractable(2);
        });

        Observable.Interval(new TimeSpan(0, 0, spawnInterval)).TakeUntilDestroy(gameObject).Subscribe(_ =>
        {
            if(speedSetter.CurrentSpeed>0) SpawnInteractable(1);
        });
        Observable.Timer(new TimeSpan(0, 0, spawnInterval / 2)).TakeUntilDestroy(gameObject).Subscribe(_ =>
        {
            Observable.Interval(new TimeSpan(0, 0, spawnInterval)).TakeUntilDestroy(gameObject).Subscribe(_ =>
            {
                if (speedSetter.CurrentSpeed > 0) SpawnInteractable(2);
            });
        });
    }


    public void SpawnInteractable(int index)
    {
        float x = UnityEngine.Random.Range(SpawnLimits[0], SpawnLimits[1]);
        Vector3 spawnPoint = new Vector3(x, SpawnY, SpawnZ);

        BaseInteractable interactable = PoolManager.Get(index).GetComponent<BaseInteractable>();
        interactable.transform.position = spawnPoint;
        speedSetter.SpeedUpInteractable(interactable, speedSetter.RequiredSpeed);

        interactables.Add(interactable);
    }
}
